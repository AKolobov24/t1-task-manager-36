package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;
import java.util.UUID;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @Test
    public void add() {
        @NotNull ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull Task task = createTask();
        repository.add(USER1_ID, task);
        task.setUserId(USER1_ID);
        Assert.assertEquals(task, repository.findAll().get(0));
        repository.add(USER_EMPTY_ID, task);
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull ITaskRepository repository = new TaskRepository();
        List<Task> taskList = createTaskList(USER1_ID);
        repository.add(taskList);
        Task user2Task = repository.add(createTask(USER2_ID));
        Assert.assertEquals(taskList.size() + 1, repository.findAll().size());
        repository.clear(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Task, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull ITaskRepository repository = new TaskRepository();
        @NotNull String taskId = repository.add(createTask(USER1_ID)).getId();
        Assert.assertTrue(repository.existById(USER1_ID, taskId));
        Assert.assertFalse(repository.existById(USER2_ID, taskId));
    }

    @Test
    public void findAll() {
        @NotNull ITaskRepository repository = new TaskRepository();
        List<Task> user1TaskList = createTaskList(USER1_ID);
        List<Task> user2TaskList = createTaskList(USER2_ID);
        repository.add(user1TaskList);
        repository.add(user2TaskList);
        Assert.assertEquals(user1TaskList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull ITaskRepository repository = new TaskRepository();
        @NotNull Task task = repository.add(createTask(USER1_ID));
        @NotNull String taskId = task.getId();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, taskId));
        Assert.assertNull(repository.findOneById(USER2_ID, taskId));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void findOneByIndex() {
        @NotNull ITaskRepository repository = new TaskRepository();
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        int lastIndex = repository.getSize() - 1;
        Assert.assertEquals(task, repository.findOneByIndex(USER1_ID, lastIndex));
        repository.findOneByIndex(USER1_ID, lastIndex + 1);
    }

    @Test
    public void getSize() {
        @NotNull ITaskRepository repository = new TaskRepository();
        List<Task> taskList = createTaskList(USER1_ID);
        repository.add(taskList);
        Assert.assertEquals((Integer) taskList.size(), repository.getSize(USER1_ID));
        repository.add(createTask(USER1_ID));
        Assert.assertEquals((Integer) (taskList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull ITaskRepository repository = new TaskRepository();
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        Assert.assertNull(repository.remove(USER_EMPTY_ID, task));
        Assert.assertEquals(task, repository.findOneById(USER1_ID, task.getId()));
        repository.remove(USER1_ID, task);
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        @NotNull ITaskRepository repository = new TaskRepository();
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        repository.removeById(USER1_ID, task.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
        repository.removeById(USER2_ID, task.getId());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndex() {
        @NotNull ITaskRepository repository = new TaskRepository();
        List<Task> taskList = createTaskList(USER1_ID);
        repository.add(taskList);
        int indexForCheck = taskList.size() - 2;
        @NotNull Task task = repository.findOneByIndex(USER1_ID, indexForCheck);
        Assert.assertEquals(task, repository.removeByIndex(USER1_ID, indexForCheck));
        Assert.assertEquals((Integer) (taskList.size() - 1), repository.getSize());
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
        repository.removeByIndex(USER1_ID, taskList.size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull ITaskRepository repository = new TaskRepository();
        String projectId = UUID.randomUUID().toString();
        List<Task> taskList = createTaskList(USER1_ID);
        taskList.forEach(t -> t.setProjectId(projectId));
        repository.add(taskList);
        repository.add(createTask(USER1_ID));
        Assert.assertEquals(taskList, repository.findAllByProjectId(USER1_ID, projectId));
    }

}
