package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestSession.createSession;
import static ru.t1.akolobov.tm.data.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @Test
    public void add() {
        @NotNull ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull Session session = createSession(USER1_ID);
        repository.add(USER1_ID, session);
        Assert.assertEquals(session, repository.findAll().get(0));
        repository.add(USER_EMPTY_ID, session);
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull ISessionRepository repository = new SessionRepository();
        List<Session> sessionList = createSessionList(USER1_ID);
        repository.add(sessionList);
        Session user2Session = repository.add(createSession(USER2_ID));
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        repository.clear(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull ISessionRepository repository = new SessionRepository();
        @NotNull String sessionId = repository.add(createSession(USER1_ID)).getId();
        Assert.assertTrue(repository.existById(USER1_ID, sessionId));
        Assert.assertFalse(repository.existById(USER2_ID, sessionId));
    }

    @Test
    public void findAll() {
        @NotNull ISessionRepository repository = new SessionRepository();
        List<Session> user1SessionList = createSessionList(USER1_ID);
        List<Session> user2SessionList = createSessionList(USER2_ID);
        repository.add(user1SessionList);
        repository.add(user2SessionList);
        Assert.assertEquals(user1SessionList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull ISessionRepository repository = new SessionRepository();
        @NotNull Session session = repository.add(createSession(USER1_ID));
        @NotNull String sessionId = session.getId();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, sessionId));
        Assert.assertNull(repository.findOneById(USER2_ID, sessionId));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void findOneByIndex() {
        @NotNull ISessionRepository repository = new SessionRepository();
        repository.add(createSessionList(USER1_ID));
        int lastIndex = repository.getSize() - 1;
        @NotNull Session session = repository.add(createSession(USER1_ID));
        Assert.assertEquals(session, repository.findOneByIndex(USER1_ID, lastIndex + 1));
        repository.findOneByIndex(USER1_ID, lastIndex + 2);
    }

    @Test
    public void getSize() {
        @NotNull ISessionRepository repository = new SessionRepository();
        List<Session> sessionList = createSessionList(USER1_ID);
        repository.add(sessionList);
        Assert.assertEquals((Integer) sessionList.size(), repository.getSize(USER1_ID));
        repository.add(createSession(USER1_ID));
        Assert.assertEquals((Integer) (sessionList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull ISessionRepository repository = new SessionRepository();
        repository.add(createSessionList(USER1_ID));
        @NotNull Session session = repository.add(createSession(USER1_ID));
        Assert.assertNull(repository.remove(USER_EMPTY_ID, session));
        Assert.assertEquals(session, repository.findOneById(USER1_ID, session.getId()));
        repository.remove(USER1_ID, session);
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        @NotNull ISessionRepository repository = new SessionRepository();
        repository.add(createSessionList(USER1_ID));
        @NotNull Session session = repository.add(createSession(USER1_ID));
        repository.removeById(USER1_ID, session.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
        repository.removeById(USER2_ID, session.getId());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndex() {
        @NotNull ISessionRepository repository = new SessionRepository();
        List<Session> sessionList = createSessionList(USER1_ID);
        repository.add(sessionList);
        int indexForCheck = sessionList.size() - 2;
        @NotNull Session session = repository.findOneByIndex(USER1_ID, indexForCheck);
        Assert.assertEquals(session, repository.removeByIndex(USER1_ID, indexForCheck));
        Assert.assertEquals((Integer) (sessionList.size() - 1), repository.getSize());
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
        repository.removeByIndex(USER1_ID, sessionList.size());
    }

}
