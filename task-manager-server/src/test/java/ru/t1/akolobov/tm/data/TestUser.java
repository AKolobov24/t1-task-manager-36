package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class TestUser {

    public static final User USER1 = createUser("USER_1");
    public static final User USER2 = createUser("USER_2");
    public static final User NEW_USER = createUser("NEW_USER");
    public static final String USER_EMPTY_ID = "";
    public static final String USER1_ID = USER1.getId();
    public static final String USER2_ID = USER2.getId();

    @NotNull
    public static User createUser(@NotNull String login) {
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
        user.setEmail(login + "@email.ru");
        return user;
    }

    @NotNull
    public static List<User> createUserList() {
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull User user = new User();
            @NotNull String login = "USER_" + i;
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
            user.setEmail(login + "@email.ru");
            userList.add(user);
        }
        return userList;
    }

}
