package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.service.ISessionService;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.IndexIncorrectException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.repository.SessionRepository;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestSession.createSession;
import static ru.t1.akolobov.tm.data.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    private final ISessionRepository repository = new SessionRepository();
    private final ISessionService service = new SessionService(repository);

    @Before
    public void initRepository() {
        repository.add(createSessionList(USER1_ID));
    }

    @After
    public void clearRepository() {
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
    }

    @Test
    public void add() {
        Session session = createSession(USER1_ID);
        Object result = service.add(USER1_ID, session);
        Assert.assertNotNull(result);
        Assert.assertEquals(session, result);
        Assert.assertEquals(session, repository.findOneById(session.getUserId(), session.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, session));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        List<Session> sessionList = createSessionList(USER2_ID);
        service.add(sessionList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        Session session = createSession(USER1_ID);
        service.add(session);
        Assert.assertTrue(service.existById(USER1_ID, session.getId()));
        Assert.assertFalse(service.existById(USER2_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, session.getId()));
    }

    @Test
    public void findAll() {
        List<Session> sessionList = createSessionList(USER2_ID);
        service.add(sessionList);
        Assert.assertEquals(sessionList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }

    @Test
    public void findOneById() {
        @NotNull Session session = createSession(USER1_ID);
        service.add(session);
        Assert.assertEquals(session, service.findOneById(USER1_ID, session.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, session.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void findOneByIndex() {
        @NotNull Session session = createSession(USER1_ID);
        service.add(session);
        int indexForCheck = service.findAll(USER1_ID).indexOf(session);
        service.add(createSessionList(USER1_ID));
        Assert.assertEquals(session, service.findOneByIndex(USER1_ID, indexForCheck));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneByIndex(USER_EMPTY_ID, indexForCheck)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.findOneByIndex(USER1_ID, service.findAll(USER1_ID).size())
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createSession(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        int size = service.findAll(USER1_ID).size();
        Session session = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(session);
        service.remove(USER1_ID, session);
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, session));
    }

    @Test
    public void removeById() {
        int size = service.findAll(USER1_ID).size();
        Session session = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(session);
        service.removeById(USER1_ID, session.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    public void removeByIndex() {
        int size = service.findAll(USER1_ID).size();
        Session session = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(session);
        service.removeByIndex(USER1_ID, 0);
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(USER_EMPTY_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(USER1_ID, size));
    }

}
