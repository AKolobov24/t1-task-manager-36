package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.ISessionService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.exception.user.AccessDeniedException;
import ru.t1.akolobov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.akolobov.tm.exception.user.LoginEmptyException;
import ru.t1.akolobov.tm.exception.user.PasswordEmptyException;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.util.CryptUtil;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.Date;
import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull IUserService userService,
            @NotNull IPropertyService propertyService,
            @NotNull ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(IncorrectLoginOrPasswordException::new);
        if (user.isLocked() || !HashUtil.salt(propertyService, password).equals(user.getPasswordHash()))
            throw new IncorrectLoginOrPasswordException();
        return getToken(user);
    }

    @NotNull
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(token, sessionKey);
    }

    @NotNull
    private Session createSession(@NotNull User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        return sessionService.add(session);
    }

    @NotNull
    @SneakyThrows
    public Session validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(token, sessionKey);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);

        @NotNull Date currentDate = new Date();
        final long delta = (currentDate.getTime() - session.getDate().getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void logout(@Nullable String token) {
        if (token == null || token.isEmpty()) return;
        @NotNull final Session session = validateToken(token);
        sessionService.removeById(session.getId());
    }

}
