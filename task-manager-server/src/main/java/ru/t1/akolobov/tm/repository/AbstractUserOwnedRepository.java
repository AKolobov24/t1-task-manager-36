package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IUserOwnedRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    @Nullable
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return null;
        model.setUserId(userId);
        models.add(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<? super M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()) && userId.equals(m.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public M findOneByIndex (@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    @NotNull
    public Integer getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

    @Override
    @Nullable
    public M remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return null;
        return removeById(userId, model.getId());
    }

    private void removeAll(final @NotNull List<M> models) {
        if (models.isEmpty()) return;
        models.forEach(this.models::remove);
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final M model = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final M model = findOneByIndex(userId, index);
        models.remove(model);
        return model;
    }

}
