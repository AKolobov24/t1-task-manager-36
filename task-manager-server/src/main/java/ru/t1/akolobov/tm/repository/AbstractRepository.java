package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    @NotNull
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return models;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<? super M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @Override
    @NotNull
    public Integer getSize() {
        return models.size();
    }

    @Override
    @NotNull
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String id) {
        @NotNull final M model = Optional.ofNullable(findOneById(id))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final M model = findOneByIndex(index);
        models.remove(model);
        return model;
    }

}
