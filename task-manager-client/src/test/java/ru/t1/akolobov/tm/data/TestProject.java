package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static Project createProject() {
        return new Project("new-project", "new-project-desc");
    }

    @NotNull
    public static List<Project> createProjectList(int size) {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            @NotNull Project project = new Project("project-" + i, "project-" + i + "desc");
            projectList.add(project);
        }
        return projectList;
    }

}
