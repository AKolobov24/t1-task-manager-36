package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.ProjectCreateResponse;
import ru.t1.akolobov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.akolobov.tm.dto.response.ProjectGetByIndexResponse;
import ru.t1.akolobov.tm.dto.response.ProjectListResponse;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.marker.IntegrationCategory;
import ru.t1.akolobov.tm.model.Project;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    public static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance("localhost", "6060");
    public static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance("localhost", "6060");

    public static String adminToken;
    public static String userToken;
    public static List<Project> projectList = createProjectList(4);

    @BeforeClass
    public static void prepareSession() {
        adminToken = authEndpoint.login(new UserLoginRequest("akolobov", "akolobov")).getToken();
        createProjects(adminToken);
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
        createProjects(userToken);
    }

    public static void createProjects(@Nullable String token) {
        ProjectCreateRequest request = new ProjectCreateRequest(token);
        for (int i = 0; i < 4; i++) {
            request.setName(projectList.get(i).getName());
            request.setDescription(projectList.get(i).getDescription());
            projectEndpoint.create(request);
        }
    }

    @AfterClass
    public static void closeSessions() {
        projectEndpoint.clear(new ProjectClearRequest(userToken));
        projectEndpoint.clear(new ProjectClearRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        adminToken = null;
        userToken = null;
    }

    @Test
    public void changeStatusById() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        @NotNull Project project = projectListResponse
                .getProjectList()
                .get(projectListResponse.getProjectList().size() - 1);
        @NotNull ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(adminToken);
        request.setId(project.getId());
        request.setStatus(Status.IN_PROGRESS);
        projectEndpoint.changeStatusById(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.changeStatusById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.changeStatusById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.changeStatusById(new ProjectChangeStatusByIdRequest())
        );
    }

    @Test
    public void changeStatusByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        @NotNull ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(adminToken);
        int index = projectListResponse.getProjectList().size() - 1;
        @NotNull Project project = projectListResponse.getProjectList().get(index);
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);
        projectEndpoint.changeStatusByIndex(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());

        request.setIndex(projectListResponse.getProjectList().size() + 2);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.changeStatusByIndex(request));
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.changeStatusByIndex(new ProjectChangeStatusByIndexRequest())
        );
    }

    @Test
    public void clear() {
        @NotNull ProjectListRequest projectListRequest = new ProjectListRequest(userToken);
        Assert.assertFalse(projectEndpoint.list(projectListRequest).getProjectList().isEmpty());
        projectEndpoint.clear(new ProjectClearRequest(userToken));
        Assert.assertTrue(projectEndpoint.list(projectListRequest).getProjectList().isEmpty());
        Assert.assertFalse(projectEndpoint.list(new ProjectListRequest(adminToken)).getProjectList().isEmpty());
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.clear(new ProjectClearRequest()));
        createProjects(userToken);
    }

    @Test
    public void completeById() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        @NotNull Project project = projectListResponse
                .getProjectList()
                .get(projectListResponse.getProjectList().size() - 1);
        @NotNull ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(adminToken);
        request.setId(project.getId());
        projectEndpoint.completeById(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.completeById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.completeById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.completeById(new ProjectCompleteByIdRequest())
        );
    }

    @Test
    public void completeByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        int index = projectListResponse.getProjectList().size() - 1;
        @NotNull Project project = projectListResponse.getProjectList().get(index);
        @NotNull ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(adminToken);
        request.setIndex(index);
        projectEndpoint.completeByIndex(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());

        request.setIndex(projectListResponse.getProjectList().size() + 2);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.completeByIndex(request));
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.completeByIndex(new ProjectCompleteByIndexRequest())
        );
    }

    @Test
    public void create() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName(project.getName());
        request.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse response = projectEndpoint.create(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(project.getName(), response.getProject().getName());
        Assert.assertEquals(project.getDescription(), response.getProject().getDescription());

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(response.getProject().getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(project.getName(), getByIdResponse.getProject().getName());
        Assert.assertEquals(project.getDescription(), getByIdResponse.getProject().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.create(new ProjectCreateRequest())
        );
    }

    @Test
    public void getById() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Assert.assertNotNull(createResponse.getProject());

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getProject().getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);

        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(createResponse.getProject().getId(), getByIdResponse.getProject().getId());
        Assert.assertEquals(project.getName(), getByIdResponse.getProject().getName());
        Assert.assertEquals(project.getDescription(), getByIdResponse.getProject().getDescription());

        getByIdRequest.setToken(adminToken);
        Assert.assertNull(projectEndpoint.getById(getByIdRequest).getProject());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.getById(new ProjectGetByIdRequest())
        );
    }

    @Test
    public void getByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(userToken)
        );

        int index = projectListResponse.getProjectList().size() - 1;
        Project project = projectListResponse.getProjectList().get(index);

        @NotNull ProjectGetByIndexRequest getByIndexRequest = new ProjectGetByIndexRequest(userToken);
        getByIndexRequest.setIndex(index);
        @NotNull ProjectGetByIndexResponse getByIndexResponse = projectEndpoint.getByIndex(getByIndexRequest);

        Assert.assertNotNull(getByIndexResponse.getProject());
        Assert.assertEquals(project.getId(), getByIndexResponse.getProject().getId());
        Assert.assertEquals(project.getName(), getByIndexResponse.getProject().getName());
        Assert.assertEquals(project.getDescription(), getByIndexResponse.getProject().getDescription());

        getByIndexRequest.setIndex(projectListResponse.getProjectList().size());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.getByIndex(getByIndexRequest));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.getByIndex(new ProjectGetByIndexRequest())
        );
    }

    @Test
    public void list() {
        ProjectListRequest userProjectListRequest = new ProjectListRequest(userToken);
        ProjectListRequest adminProjectListRequest = new ProjectListRequest(adminToken);
        int userProjectListSize = projectEndpoint
                .list(userProjectListRequest)
                .getProjectList().size();
        int adminProjectListSize = projectEndpoint
                .list(adminProjectListRequest)
                .getProjectList().size();

        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Project createdProject = createResponse.getProject();
        Assert.assertNotNull(createdProject);

        List<Project> projectList = projectEndpoint.list(userProjectListRequest).getProjectList();

        Assert.assertEquals(
                userProjectListSize + 1,
                projectList.size()
        );

        Assert.assertEquals(
                adminProjectListSize,
                projectEndpoint.list(adminProjectListRequest).getProjectList().size()
        );

        Assert.assertTrue(
                projectList.stream()
                        .anyMatch(m -> createdProject.getId().equals(m.getId()))
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.list(new ProjectListRequest())
        );
    }

    @Test
    public void removeById() {
        ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(userToken);
        projectGetByIndexRequest.setIndex(0);
        Project project = projectEndpoint.getByIndex(projectGetByIndexRequest).getProject();
        Assert.assertNotNull(project);

        ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(userToken);
        projectRemoveByIdRequest.setId(project.getId());
        projectEndpoint.removeById(projectRemoveByIdRequest);
        Assert.assertFalse(projectEndpoint
                .list(new ProjectListRequest(userToken))
                .getProjectList()
                .contains(project)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.removeById(new ProjectRemoveByIdRequest())
        );
    }

    @Test
    public void removeByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(userToken)
        );

        int size = projectListResponse.getProjectList().size();
        int index = size - 1;
        Project project = projectListResponse.getProjectList().get(index);

        @NotNull ProjectRemoveByIndexRequest removeByIndexRequest =
                new ProjectRemoveByIndexRequest(userToken);
        removeByIndexRequest.setIndex(index);
        projectEndpoint.removeByIndex(removeByIndexRequest);

        Assert.assertEquals(
                size - 1,
                projectEndpoint.list(new ProjectListRequest(userToken))
                        .getProjectList().size()
        );
        ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(project.getId());
        Assert.assertNull(projectEndpoint.getById(getByIdRequest).getProject());

        removeByIndexRequest.setIndex(projectListResponse.getProjectList().size());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.removeByIndex(removeByIndexRequest)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.removeByIndex(new ProjectRemoveByIndexRequest())
        );
    }

    @Test
    public void startById() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        @NotNull Project project = projectListResponse
                .getProjectList()
                .get(projectListResponse.getProjectList().size() - 1);
        @NotNull ProjectStartByIdRequest request = new ProjectStartByIdRequest(adminToken);
        request.setId(project.getId());
        projectEndpoint.startById(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.startById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.startById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.startById(new ProjectStartByIdRequest())
        );
    }

    @Test
    public void startByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(adminToken)
        );
        int index = projectListResponse.getProjectList().size() - 1;
        @NotNull Project project = projectListResponse.getProjectList().get(index);

        @NotNull ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(adminToken);
        request.setIndex(index);
        projectEndpoint.startByIndex(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());

        request.setIndex(projectListResponse.getProjectList().size() + 2);
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.startByIndex(request));
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.startByIndex(new ProjectStartByIndexRequest())
        );
    }

    @Test
    public void updateById() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());
        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Assert.assertNotNull(createResponse.getProject());

        @NotNull String name = "update-by-id-name";
        @NotNull String description = "update-by-id-desc";
        @NotNull ProjectUpdateByIdRequest updateByIdRequest = new ProjectUpdateByIdRequest(userToken);
        updateByIdRequest.setId(createResponse.getProject().getId());
        updateByIdRequest.setName(name);
        updateByIdRequest.setDescription(description);
        projectEndpoint.updateById(updateByIdRequest);

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getProject().getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(name, getByIdResponse.getProject().getName());
        Assert.assertEquals(description, getByIdResponse.getProject().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.updateById(new ProjectUpdateByIdRequest())
        );
        updateByIdRequest.setId(project.getId());
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.updateById(updateByIdRequest)
        );
    }

    @Test
    public void updateByIndex() {
        @NotNull ProjectListResponse projectListResponse = projectEndpoint.list(
                new ProjectListRequest(userToken)
        );
        int index = projectListResponse.getProjectList().size() - 1;
        @NotNull Project project = projectListResponse.getProjectList().get(index);

        @NotNull String name = "update-by-id-name";
        @NotNull String description = "update-by-id-desc";
        @NotNull ProjectUpdateByIndexRequest updateByIndexRequest = new ProjectUpdateByIndexRequest(userToken);
        updateByIndexRequest.setIndex(index);
        updateByIndexRequest.setName(name);
        updateByIndexRequest.setDescription(description);
        projectEndpoint.updateByIndex(updateByIndexRequest);

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(name, getByIdResponse.getProject().getName());
        Assert.assertEquals(description, getByIdResponse.getProject().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.updateByIndex(new ProjectUpdateByIndexRequest())
        );
        updateByIndexRequest.setIndex(projectListResponse.getProjectList().size());
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.updateByIndex(updateByIndexRequest)
        );
    }

}
